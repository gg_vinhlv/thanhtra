import React from 'react';

class Footer extends React.Component {
 
  render() {
    return (
      <footer className="footer-container">
        <div className="title">
        	Hệ thống CSDL Tiếp công dân và Giải quyết đơn trên địa bàn Thành phố Đà Nẵng
        </div>
        <div className="address">
        	Địa chỉ : 24 Trần Phú, Quận Hải Châu, TP. Đà Nẵng - 2017 © bởi Thanh tra Thành phố
        </div>
      </footer>
    );
  }
}
export default Footer;
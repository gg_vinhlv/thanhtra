import React from 'react';
import { Select } from "@blueprintjs/core";

class Home extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      type:'1'
    }
  }
  handleChange(event, key) {
    console.log(event, key)
    this.setState({[key]: event.target.value});
  }

  render() {
    return (
      <div className="search-form">
        <label className="pt-label " >
          Mã đơn          
          <input className="pt-input"  type="text" />
        </label>
        <label className="pt-label " >
          Tên người đứng đơn  
          <input className="pt-input" type="text" />
        </label>
        <label className="pt-label " >
          Phân loại đơn
          <div className="pt-select">
            <select value={this.state.type} onChange={this.handleChange.bind(this,'type')} style={{}} >
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
              <option value="4">Four</option>
            </select>
          </div>
        </label>
        <label className="pt-label " >
          Loại tiếp công dân
          <div className="pt-select">
            <select value={this.state.type} onChange={this.handleChange.bind(this,'type')}>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
              <option value="4">Four</option>
            </select>
          </div>
        </label>
      </div>
    );
  }
}

export default Home;